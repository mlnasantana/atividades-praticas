# Atividades Praticas

**Questão 3**

O código é a representação de uma calculadora, realizando uma soma de dois valores disponibilizados pelo usuário através do terminal.
Para compilar o codigo em Js: 
node <nomeDoPrograma> <entrada1> <entrada2> node calculadora.js 2 2
resultado: 4

**Questão 4**

Para conseguir subir um codigo em um repositório sem usar o git add. é utilizado o comando "git add <nomeDoArquivo>", e dessa forma é enviado 
somente o arquivo desejado, e não todas as alterações.
Com isso, basta realizar os seguintes comandos:
`git add calculadora.js`
`git add README.md`

**Questão 6**

Existem dois erros nesse codigo.

 Na linha 21, está escrito arg[0], faltando o “S”.
Corrigindo ficaria: args[0];

E os parâmetros passados args 2 - ser args[1] ao invés de args[2] e 
ao invés de args[0] e ao invés de args[1], pois no args[0] está sendo 
armazenado a opção para o switch, sendo definido como operação que 
a calculadora deveria realizar.

**Questão 10**

Antes de executar adicionei ao código a linha:  -> const args = process.argv.slice(2); 
Para execução compilo o comando: node calculadora.js 'numero' ('/' ou '+' ou '-') 'numero'  - A função Eval avalia a expressão de cadeia de caracteres e seu valor.




